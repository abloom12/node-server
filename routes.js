const fs = require('fs');

const requestHandler = (req, res) => {
  // Client => Request => Server => Response => Client
  const url = req.url;
  const method = req.method;

  if (url === '/') {
    res.write('<html>');
    res.write('<head><title>Send A Message</title></head>');
    res.write('<body>');
    res.write('<form action="/message" method="POST">');
    res.write('<input type="text" name="message">');
    res.write('<button type="submit">Send</button>');
    res.write('</form>');
    res.write('</body>');
    res.write('</html>');
    return res.end();
  }
  
  if (url === '/message' && method === 'POST') {
    // parsing request data in chunks
    const body = [];
    req.on('data', (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
  
    return req.on('end', () => {
      const parsedBody = Buffer.concat(body).toString();
      const message = parsedBody.split('=')[1];
      fs.writeFile('message.txt', message, (error) => {
        res.statusCode = 302; // 302 is for redirects
        res.setHeader('Location', '/'); // where to redirect
        return res.end();
      });
    });
  }
  
  res.setHeader('Content-Type', 'text/html');
  res.write('<html>');
  res.write('<head><title>Servers with Node.js</title></head>');
  res.write('<body>');
  res.write('<h1>Hello from Node.js Server');
  res.write('</body>');
  res.write('</html>');
  res.end();
};


module.exports = requestHandler;

// Other ways to export
//----------------------
// module.exports.handler = requestHandler;

// module.exports = {
//   handler: requestHandler
// }